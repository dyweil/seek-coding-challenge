# Seek Coding Exercise

## Overview

- This coding test is written in Typescript
- To play around with the program, feel free to modify **./src/main.ts** file
- To run the program, please run the following commands
  - `npm i`
  - `npm start`
- Typescript provides a certain degree of type-safety and is useful for this test
- With type support in Typescript, type matching can be used when dealing with different rules
- If not all rules are handled in doing the total amount calculation, it will not compile
  (given that `PricingRules` in **./rules.ts** is a union type of all available rules)
- `Rule.type` is used to distinguish between different rules
- Error handling is quite generic - could have been more granular
- ES2015 Maps are used extensively throughout the program for quick look up

## Pricing rules implementation

2 types of pricing rules are divised from the specifications.

- **DiscountRule**: This rule can be added to allow staff member to configure
the price for an item regardless of the quantity. 
Discount is given based on the acutal item quantity in the cart (instead of effective item quantity).
- **MoreForLessRule**: This rule can be added to allow staff member to configure
an individual price of an item when buying in a LOT (of a certain amount).

When setting up a rule, a helper method is exported to create an instance of a rule.
When creating an instance of a rule, it validates the input and throws Error if the input is not valid.
The validation checks for the product data source as well (e.g. is the product code valid).
This can actually be setup such that the program doesn't throw an Error if the validation (against data source) fails
which could be handled gracefully when applying rules to the *cart*. 

## Assumptions

A few assumptions are made when developing this program.

- One type of rule can only be applied once to a product
- Multiple types of rules can be applied to a product
- Products that are unknown to the systems are ignored
- In the **MoreForLessRule**, the lot size has to be at least 2

## Unit tests

_Mocha_ and _Chai_ are used for unit testing the code. 
To run the unit tests, please run the following steps:
- `npm i`
- `npm test`

The tests cover a few use cases, including the one in the coding test specification page. 
The tests also test for edge cases.