import products from './products';
import { EffectiveItemRules } from './rules';

interface CartItem {
  quantity: number;
  price: number;
}

// manages items added by the customer
export default class Cart {
  // items are managed in a Map for fast lookup (o(1))
  private _items: Map<string, CartItem>;

  constructor() {
    this._items = new Map();
  }

  addItem(productCode: string) {
    const product = products[productCode];

    // ignoring product that doesn't exist
    // alternatively, an Error can be thrown if unexpected products are added
    if (!product) return;

    const cartItem = this._items.get(productCode);
    if (!cartItem)
      this._items.set(productCode, { price: product.retailPrice, quantity: 1 });
    else
      // Create a new CartItem object without mutation
      // I can also mutate the object directly which doesn't affect the functionality of the application
      this._items.set(productCode, Object.assign({}, cartItem, { quantity: cartItem.quantity + 1 }))
  }

  getItemCount(productCode: string): number {
    const cartItem = this._items.get(productCode);

    if (!cartItem)
      return 0;

    return cartItem.quantity;
  }

  applyRules(effectiveItemRules: EffectiveItemRules): Map<string, CartItem> {
    return [...this._items.keys()].reduce((acc, productCode) => {
      
      // should never not be undefined
      // should not be mutated!
      const cartItem = this._items.get(productCode)!; 

      let effectiveCartItem = Object.assign({}, cartItem);

      if (!acc.has(productCode))
        acc.set(productCode, Object.assign({}, effectiveCartItem));
        
      // time to apply rules
      const itemRules = effectiveItemRules.Rules.get(productCode);
      if (itemRules && itemRules.size) {
        for (const rule of itemRules.values()) {
          // this feature is so cool - it knows what type it's dealing with
          // and blows up in compile time if not all rules are handled
          switch (rule.type) {
            case 'DiscountRule':
              const price = rule.effectivePrice;
              effectiveCartItem = Object.assign({}, effectiveCartItem, { price });
              break;
            case 'MoreForLessRule':
              const quantity = Math.floor(effectiveCartItem.quantity / rule.lotQuantity) * rule.effectiveQuantity 
                + effectiveCartItem.quantity % rule.lotQuantity;
              effectiveCartItem = Object.assign({}, effectiveCartItem, { quantity });
              break;
            default:
              nonExhaustiveRuleHandling(rule);
          }

          acc.set(productCode, effectiveCartItem);
        }
      }
      
      return acc;
    }, new Map<string, CartItem>());
  }
}

function nonExhaustiveRuleHandling(rule: never): never {
  throw new Error('Not all rules and handled');
}