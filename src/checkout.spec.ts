import { expect } from "chai";
import { describe, it } from "mocha";

import Checkout from "./checkout";
import { newMoreForLessRule, PricingRule, newDiscountRule } from "./rules";

describe("Standard tests - based on code challenge specifications", () => {
  it("Test spec for default", () => {
    // default without any pricing rules apply
    const co = new Checkout();
    co.scan("classic");
    co.scan("standout");
    co.scan("premium");

    expect(co.total()).to.equal(987.97);
  });

  it("Test spec for SecondBite", () => {
    const pricingRules: PricingRule[] = [
      newMoreForLessRule('classic', 3, 2),
    ];
    const co = new Checkout(pricingRules);
    co.scan('classic');
    co.scan('classic');
    co.scan('classic');
    co.scan('premium');

    expect(co.total()).to.equal(934.97);
  });

  it("Test spec for Axil Coffee Roasters", () => {
    const pricingRules: PricingRule[] = [
      newDiscountRule('standout', 299.99),
    ];
    const co = new Checkout(pricingRules);
    co.scan('standout');
    co.scan('standout');
    co.scan('standout');
    co.scan('premium');

    expect(co.total()).to.equal(1294.96);
  });

  it("Test spec for MYER", () => {
    const pricingRules: PricingRule[] = [
      newMoreForLessRule('standout', 5, 4),
      newDiscountRule('premium', 389.99),
    ];
    const co = new Checkout(pricingRules);
    co.scan('standout');
    co.scan('standout');
    co.scan('standout');
    co.scan('standout');
    co.scan('standout');
    co.scan('standout');
    co.scan('standout');
    co.scan('classic');
    co.scan('classic');
    co.scan('premium');
    co.scan('premium');

    expect(co.total()).to.equal(3257.9);
  });
});

describe('Rule creation validation tests', () => {
  it('Do not allow discounted price to be greater or equal to retail price', () => {
    try {
      const pricingRules = [newDiscountRule('classic', 400)];
    } catch (err) {
      expect(err.message).to.equals('InvalidPriceInput');
    }
  });

  it('Do not allow adding rule to invalid product', () => {
    try {
      const pricingRules = [newDiscountRule('oliver', 400)];
    } catch (err) {
      expect(err.message).to.equals('ProductNotFound');
    }
  });

  it('DiscountRule: Error when discounted price is greater than retail price', () => {
    try {
      const pricingRules = [newDiscountRule('premium', 1000)];
    } catch (err) {
      expect(err.message).to.equals('InvalidPriceInput');
    }
  });

  it('DiscountRule: Error when discounted price < 0', () => {
    try {
      const pricingRules = [newDiscountRule('premium', -1)];
    } catch (err) {
      expect(err.message).to.equals('InvalidPriceInput');
    }
  });

  it('MoreForLessRule: Error when lot quantity is < 2', () => {
    try {
      const pricingRules = [newMoreForLessRule('premium', 1, 1)];
    } catch (err) {
      expect(err.message).to.equals('InvalidQuantityInput');
    }
  });

  it('MoreForLessRule: Error when effective quantity is < 1', () => {
    try {
      const pricingRules = [newMoreForLessRule('premium', 2, 0)];
    } catch (err) {
      expect(err.message).to.equals('InvalidQuantityInput');
    }
  });

  it('MoreForLessRule: Error when effective quantity is greater than lot quantity', () => {
    try {
      const pricingRules = [newMoreForLessRule('premium', 2, 3)];
    } catch (err) {
      expect(err.message).to.equals('InvalidQuantityInput');
    }
  });
});

describe('Other test cases', () => {
  it('2 different rules apply to a product type', () => {
    const pricingRules: PricingRule[] = [
      newMoreForLessRule('premium', 3, 2),
      newDiscountRule('premium', 300),
    ];

    const co = new Checkout(pricingRules);
    co.scan('premium');
    co.scan('premium');
    co.scan('premium');
    co.scan('premium');
    co.scan('premium');
    co.scan('premium');

    expect(co.total()).to.equal(1200);
  });

  it("Should not include products that do not exist", () => {
    const co = new Checkout();
    co.scan("sparkling");
    co.scan("sparkling");
    co.scan("cheapest");
    co.scan("cheapest");
    co.scan("premium");

    expect(co.total()).to.equal(394.99);
  });

  it("2 rules of same type apply to a product type", () => {
    const pricingRules: PricingRule[] = [
      newDiscountRule('premium', 300),
      newDiscountRule('premium', 310),
    ];
    const co = new Checkout(pricingRules);
    co.scan('premium');
    co.scan('premium');
    co.scan('premium');
    co.scan('premium');
    co.scan('premium');
    co.scan('premium');

    expect(co.total()).to.equal(1800);
  });
});