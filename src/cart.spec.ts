import { expect } from 'chai';
import { describe, it } from 'mocha';
import Cart from './cart';

describe('Cart items counter tests', () => {
  it('Product item count should return the number of times a product type is added', () => {
    const cart = new Cart();

    const num = 9;

    for (let i = 0; i < num; i++) {
      cart.addItem('classic');
    }

    expect(cart.getItemCount('classic')).to.equal(9);
  });

  it('Should return 0 if the product is never added', () => {
    const cart = new Cart();

    cart.addItem('classic');

    expect(cart.getItemCount('classic')).to.equal(1);
    expect(cart.getItemCount('standout')).to.equal(0);
  });

  it('Should return 0 if the invalid product is added', () => {
    const cart = new Cart();

    cart.addItem('sparkling');
    cart.addItem('sparkling');
    cart.addItem('sparkling');

    expect(cart.getItemCount('sparkling')).to.equal(0);
  });
});