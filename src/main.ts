import Checkout from './checkout';
import { newDiscountRule, newMoreForLessRule, PricingRule } from './rules';

// default
const co = new Checkout();
co.scan('classic');
co.scan('standout');
co.scan('premium');

// SecondBite
// const pricingRules: PricingRule[] = [
//   newMoreForLessRule('classic', 3, 2),
// ];
// const co = new Checkout(pricingRules);
// co.scan('classic');
// co.scan('classic');
// co.scan('classic');
// co.scan('premium');

// Axil Coffee Roasters
// const pricingRules: PricingRule[] = [
//   newDiscountRule('standout', 299.99),
// ];
// const co = new Checkout(pricingRules);
// co.scan('standout');
// co.scan('standout');
// co.scan('standout');
// co.scan('premium');

// MYER
// const pricingRules: PricingRule[] = [
//   newMoreForLessRule('standout', 5, 4),
//   newDiscountRule('premium', 389.99),
// ];
// const co = new Checkout(pricingRules);
// co.scan('standout');
// co.scan('standout');
// co.scan('standout');
// co.scan('standout');
// co.scan('standout');
// co.scan('standout');
// co.scan('standout');
// co.scan('classic');
// co.scan('classic');
// co.scan('premium');
// co.scan('premium');

const total = co.total();
console.log(total);