import products from './products';

interface RuleBase {
  productCode: string;
  type: string;
}

// alternative: use class instead of interface
// this rule specifies the requirements for '3 for the price of 2' pricing rule
interface MoreForLessRule extends RuleBase {
  type: "MoreForLessRule";
  lotQuantity: number;
  effectiveQuantity: number;
}
// helper function to create MoreForLessRule
export function newMoreForLessRule(productCode: string, lotQuantity: number, effectiveQuantity: number): MoreForLessRule {
  if (!products[productCode])
    throw new Error('ProductNotFound');

  if (lotQuantity < 2 
    || effectiveQuantity < 1 
    || effectiveQuantity > lotQuantity)
    throw new Error('InvalidQuantityInput')

  return { productCode, lotQuantity, effectiveQuantity, type: 'MoreForLessRule' };
}

// alternative: use class instead of interface
// this rule specifies the requirements for 'discount' pricing rule
interface DiscountRule extends RuleBase {
  type: "DiscountRule";
  effectivePrice: number;
}
// helper function to create DiscountRule
// do not allow discounted price to be higher or equal to retail price
export function newDiscountRule(productCode: string, effectivePrice: number ): DiscountRule {
  if (!products[productCode]) 
    throw new Error('ProductNotFound');

  if (effectivePrice < 0 || effectivePrice >= products[productCode].retailPrice)
    throw new Error('InvalidPriceInput');
  
  return { productCode, effectivePrice, type: 'DiscountRule' };
}

export type PricingRule = MoreForLessRule | DiscountRule;

// Maps are used here for quick look up
type ItemRuleMaps = Map<string, Map<string, PricingRule>>;
export class EffectiveItemRules {
  private _rules: ItemRuleMaps;

  constructor(pricingRules: PricingRule[]) {
    this._rules = this._processPricingRule(pricingRules);
  }

  // getter for effective rules - do not allow rules to be overriden once initialized
  get Rules() {
    return this._rules;
  }

  // rules are added and organized based on product type
  // assumption: only allow one type of rule to be added to one type of product
  //             if same type of rules are added twice, only the first will be considered
  private _processPricingRule(pricingRules: PricingRule[]): ItemRuleMaps {
    return pricingRules.reduce((res, rule) => {
      let itemRules = res.get(rule.productCode);

      if (!itemRules) {
        itemRules = new Map();
        res.set(rule.productCode, itemRules);
      }

      // only add rule type that do not yet exist against a product
      if (!itemRules.has(rule.type)) {
        itemRules.set(rule.type, rule);
      }

      return res;
    }, new Map<string, Map<string, PricingRule>>());
  }
}