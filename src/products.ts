export interface Product {
  name: string;
  description: string;
  retailPrice: number;
}

const products: { [key in string]: Product } = {
  "classic": { 
    name: "Classic Ad",
    description: "Offers the most basic level of advertisement",
    retailPrice: 269.99
  },
  "standout": { 
    name: "Stand Out Ad",
    description: "Allows advertisers to use a company logo and use a longer presentation text",
    retailPrice: 322.99
  },
  "premium": { 
    name: "Premium Ad",
    description: "Same benefits as Standout Ad, but also puts the advertisement at the top of the results, allowing higher visibility",
    retailPrice: 394.99
  }
}

export default products;