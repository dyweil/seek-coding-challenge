import Cart from './cart';
import { PricingRule, EffectiveItemRules } from './rules';

export default class Checkout {
  private _cart: Cart;
  private _effectiveRules: EffectiveItemRules;

  constructor(pricingRules: PricingRule[] = []) {
    this._cart = new Cart();
    this._effectiveRules = new EffectiveItemRules(pricingRules);
  }

  scan(productCode: string): void {
    this._cart.addItem(productCode);
  }

  total(): number {
    // get effective 'cart'
    const effectiveCart = this._cart.applyRules(this._effectiveRules);

    // returns total
    return [...effectiveCart.values()].reduce((total, cartItem) => {
      return total + cartItem.price * cartItem.quantity;
    }, 0);
  }
}